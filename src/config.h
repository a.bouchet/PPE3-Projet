/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "generateurplaylist"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "XX"

/* Define to the full name of this package. */
#define PACKAGE_NAME "GenerateurPlaylist"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "GenerateurPlaylist 0.0.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "generateurplaylist"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.0.1"

/* Version number of package */
#define VERSION "0.0.1"
