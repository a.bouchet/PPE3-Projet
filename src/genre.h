#ifndef GENRE_H
#define GENRE_H
#include <iostream>

/**
 * \class Genre
 * \brief Definition of the class Genre
 */
class Genre
{
 public:

  /**
  * \brief Default constructor of Genre
  */
  Genre();

  /**
   * \brief Constructor with parameters
   * \param id the identification number of a genre
   * \param type the type of a genre
   */
  Genre(unsigned int, std::string);

  /**
   * \brief Default destructor of Genre
   */
  ~Genre();

  /**
   * \brief Lets you know the id of a genre
   * \return unsigned int representing the id of a genre
   */
  unsigned int getId();

  /**
   * \brief Lets you know the type of a genre
   * \return std::string representing the type of a genre
   */
  std::string getType();

  /**
   * \brief to set the type of a genre
   */
  void setType(std::string);

 private:
  unsigned int id; ///< Attribute defining the id of the genre
  std::string type; ///< Attribute defining the type of a genre
};

#endif // GENRE_H
