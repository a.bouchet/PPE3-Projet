#ifndef ALBUM_H
#define ALBUM_H
#include <iostream>
#include <chrono>

/**
 * \class Album
 * \brief Definition of the class Album
 */
class Album
{
 public:

  /**
   * \brief Default Constructor of Album
   */
  Album();

  /**
   * \brief Constructor with parameters
   * \param id the identification number of an album
   * \param name name of an album
   * \param date release date of an album
   */
  Album(unsigned int, std::string, std::chrono::system_clock);

  /**
  * \brief Default Destructor of Album
  */
  ~Album();

  /**
   * \brief Lets you know the ID of an album
   * \return unsigned int represents the ID of an album
   */
  unsigned int getId();

  /**
   * \brief Lets you know the name of an album
   * \return std::string represents the name of an album
   */
  std::string getName();

  /**
   * \brief to set the name of an album
   */
  void setName(std::string);

  /**
   * \brief Lets you know the date of an album
   * \return system_clock represents the date of an album
   */
  std::chrono::system_clock getDate();

  /**
   * \brief to set the name of an album
   */
  void setDate(std::chrono::system_clock);

 private:
  unsigned int id; ///< Attribute defining the ID of an album
  std::string name; ///<Attribute determining the name of an album
  std::chrono::system_clock date; ///< Attribute determining the release date of an album
};

#endif // ALBUM_H
