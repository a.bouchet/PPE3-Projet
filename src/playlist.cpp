#include "playlist.h"
#include <fstream>
#include <libpq-fe.h>

Playlist::Playlist():
  id(0),
  title(""),
  duration(0)
{}

Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::minutes _duration):
  id(_id),
  title(_title),
  duration(_duration)
{}

Playlist::~Playlist()
{}

unsigned int Playlist::getId()
{
  return id;
}

std::chrono::minutes Playlist::getDuration()
{
  return std::chrono::duration_cast<std::chrono::minutes>(duration);
}

void Playlist::setDuration(std::chrono::minutes _duration)
{
  duration = _duration;
}

void Playlist::addTrack(Track t)
{
  tracks.push_back(t);
}

void Playlist::writeM3U()
{
  std::string filename = title + ".m3u";
  {
    std::ofstream m3u_stream(filename, std::ios::out);

    if(m3u_stream.is_open())
    {
//      m3u_stream << "#EXTM3U" << std::endl;
      for(Track current_track : tracks)
      {
//        m3u_stream << "#EXTINF:" << current_track.getDuration() << ", " << current_track.getArtists() << " - " << current_track.getName() << std::endl;
        m3u_stream << current_track.getPath() << std::endl;
      }
    }
  }
}

void Playlist::writeXSPF()
{}

void Playlist::DoIt(std::string requete)
{
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 user=a.bouchet dbname=Cours password=P@ssword";
  PGPing ping = PQping(info_connexion);

  if(ping == PQPING_OK)
  {
    PGconn *connexion = PQconnectdb(info_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::chrono::seconds durationReal(0);
      std::chrono::seconds marge = duration * 20 / 100;

      while(marge > durationReal)
      {
//        std::string requete = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"artiste_morceau\" ON \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" ON \"artiste_morceau\".id_artiste = \"artiste\".id ORDER BY random() LIMIT 1;";
        // exec
        PGresult *res = PQexec(connexion, requete.c_str());

        if(PQresultStatus(res) == PGRES_TUPLES_OK)
        {
          durationReal += std::chrono::seconds(((std::atol(PQgetvalue(res, 0, 0)))));

          if(durationReal < duration)
          {
            Track t;
            // valoriser
            t.setDuration(std::chrono::seconds(std::atol(PQgetvalue(res, 0, 0))));
            t.setName(std::string(PQgetvalue(res, 0, 2)));
            t.setPath(std::string(PQgetvalue(res, 0, 3)));
            addTrack(t);
            std::string unArtist = std::string(PQgetvalue(res, 0, 1));
            Artist a;
            a.setName(unArtist);
            t.getArtists().push_back(a);
          }
        }
      }
    }
  }
}
