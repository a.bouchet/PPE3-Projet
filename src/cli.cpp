#include "cli.h"

DEFINE_uint64(duration, 4000, "Duration playlist in minute");
DEFINE_string(genre, "", "Genre of the music");
DEFINE_string(type, "", "Format of the playlist");
DEFINE_string(title, "", "Title of the music");
DEFINE_string(name, "", "Name of the playlist");
DEFINE_string(subgenre, "", "Subgenre of the music");
DEFINE_string(artist, "", "Artist name of the music");
DEFINE_string(album, "", "Album Name");
