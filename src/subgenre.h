#ifndef SUBGENRE_H
#define SUBGENRE_H
#include <iostream>

/**
 * \class Subgenre
 * \brief Definition of the class Subgenre
 */
class Subgenre
{
 public:

  /**
   * \brief Default constructor of Subenre
   */
  Subgenre();

  /**
  * \brief Constructor with parameters
  * \param id the id of a subgenre
  * \param sub_type the sub type of a subgenre
  */
  Subgenre(unsigned int, std::string);

  /**
   * \brief Default destructor of Subgenre
   */
  ~Subgenre();

  /**
   * \brief Lets you know the id of a subgenre
   * \return unsigned int representing the id of a subgenre
   */
  unsigned int getId();

  /**
  * \brief Lets you know the type of a subgenre
  * \return std::string representing the sub type of a subgenre
  */
  std::string getSub_type();

  /**
   * \brief to set the sub type of a subgenre
   */
  void setSub_type(std::string);

 private:
  unsigned int id; ///< Attribute defining the id of the subgenre
  std::string sub_type; ///< Attribute defining the sub_type of the subgenre
};
#endif // SUBGENRE_H
