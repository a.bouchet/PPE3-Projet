#ifndef POLYPHONY_H
#define POLYPHONY_H
#include <iostream>

/**
 * \class Polyphony
 * \brief Definition of the class Polyphony
 */
class Polyphony
{
 public:

  /**
   * \brief Default constructor of Polyphony
   */
  Polyphony();

  /**
   * \brief Constructor with parameters
   * \param id the identification number of a polyphony
   * \param number the number of canal of a polyphony
   */
  Polyphony(unsigned int, unsigned int);

  /**
   * \brief Default destructor of Polyphony
   */
  ~Polyphony();

  /**
   * \brief Lets you know the id of a polyphony
   * \return unsigned int representing the id of a polyphony
   */
  unsigned int getId();

  /**
   * \brief Lets you know the number of canal of a polyphony
   * \return unsigned int representing the number of canal of a polyphony
   */
  unsigned int getNumber();

  /**
   * \brief to set the number of canal of a polyphony
   */
  void setNumber(unsigned int);

 private:
  unsigned int id; ///< Attribute defining the id of the polyphony
  unsigned int number; ///< Attribute defining the number of canal of the polyphony
};

#endif // POLYPHONY_H
