/**
 * \file track.h
 * \author Antoine Clara
 */

#ifndef TRACK_H
#define TRACK_H
#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include "artist.h"
#include "album.h"
#include "polyphony.h"
#include "format.h"
#include "subgenre.h"
#include "genre.h"

/**
 * \class Track
 * \brief Definition of the class Track
 */
class Track
{
 public:
  /**
   * \brief Default constructor of Track
   */
  Track();
  //  Track(unsigned int, std::string, std::string, std::chrono::duration<int>, Artist, Album, Format, Genre, Subgenre, Polyphony);
  /**
   * \brief Constructor with parameters
   * \param id the identification number of a track
   * \param duration the duration of a track
   * \param name the name of a track
   * \param path the path of a track
   * \param artists the list of artist for a track
   * \param albums the list of album for a track
   * \param polyphony the polyphony of a track
   * \param format the format of a track
   * \param subgenre the subgenre of a track
   * \param genre the genre of a track
   */
  Track(unsigned int, std::string, std::string, std::chrono::seconds, std::vector<Artist>, std::vector<Album>, Format, Genre, Subgenre, Polyphony);
  /**
   * \brief Destructor for the class Track
   */
  ~Track();

  /**
   * \brief Lets you know the id of a track
   * \return unsigned int representing the id of a track
   */
  unsigned int getId();

  /**
    * \brief Lets you know the duration of a track
    * \return int representing the duration of a track
    */
  std::chrono::seconds getDuration();

  /**
   * \brief Lets you know the name of a track
   * \return std::string representing the name of a track
   */
  std::string getName();

  /**
   * \brief Lets you know the path of a track
   * \return std::string representing the path of a track
   */
  std::string getPath();

  /**
   * \brief Lets you know one artist of the list of artist
   * \return Artist type in reference to the artist class
   */
  std::vector<Artist> getArtists();

  /**
   * \brief Lets you know one album of the list of album
   * \return Album type in reference to the Album class
   */
  std::vector<Album> getAlbums();

  /**
   * \brief Lets you know the format of a track
   * \return Artist representing the format of a track
   */
  Artist getArtist();


  
  //Album getAlbum();

  /**
   * \brief Lets you know the format of a track
   * \return Format representing the format of a track
   */
  Format getFormat();

  /**
   * \brief Lets you know the genre of a track
   * \return Genre representing the genre of a track
   */
  Genre getGenre();

  /**
   * \brief Lets you know the subgenre of a track
   * \return SubGenre representing the subgenre of a track
   */
  Subgenre getSubgenre();

  /**
   * \brief Lets you know the polyphony of a track
   * \return Polyphony representing the polyphony of a track
   */
  Polyphony getPolyphony();

  /**
   * \brief to set the duration of a track
   */
  void setDuration(std::chrono::seconds);

  /**
   * \brief to set the name of a track
   */
  void setName(std::string);

  /**
   * \brief to set the path of a track
   */
  void setPath(std::string);


 private:
  unsigned int id;///< Attribute defining the id of the track
  std::string name;///< Attribute determining the name of a track
  std::string path;///< Attribute determining the path of a track
  std::chrono::seconds duration;///< Attribute determining the duration of a track
  std::vector<Artist> artists;///< Attribute determining artists of a track
  std::vector<Album> albums;///< Attribute determining albums of a track
  Format format;///< Attribute determining format of a track
  Genre genre;///< Attribute determining genre of a track
  Subgenre subgenre;///< Attribute determining subgenre of a track
  Polyphony polyphony;///< Attribute determining polyphony of a track
};
#endif // TRACK_H
