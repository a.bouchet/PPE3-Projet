#include <libpq-fe.h>
#include <iostream>
#include "cli.h"
#include "playlist.h"
int main(int argc, char *argv[])
{
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  std::string nom = FLAGS_name;
  std::string genre = FLAGS_genre;
  std::string type = FLAGS_type;
  std::string album = FLAGS_album;
  std::string artist = FLAGS_artist;
  std::string title = FLAGS_title ;
  unsigned int duree = FLAGS_duration;
  
  std::string requete = "";
  if(genre!=""){
    requete = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"genre\" on \"morceau\".fk_genre = \"genre\".id INNER JOIN \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id  WHERE \"genre\".type='" + genre + "' ORDER BY random() LIMIT 1;";
  }
  else if (artist!=""){
    requete = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"genre\" on \"morceau\".fk_genre = \"genre\".id INNER JOIN \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id  WHERE \"artiste\".nom='" + artist + "' ORDER BY random() LIMIT 1;";
  }
  else if (album!=""){
    requete = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"genre\" on \"morceau\".fk_genre = \"genre\".id INNER JOIN \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id INNER JOIN \"album_morceau\" on \"morceau\".id = \"album_morceau\".id_morceau INNER JOIN \"album\" on \"album_morceau\".id_album = \"album\".id  WHERE \"album\".nom='" + album + "' ORDER BY random() LIMIT 1;";
  }
  else {
    requete = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"artiste_morceau\" ON \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" ON \"artiste_morceau\".id_artiste = \"artiste\".id ORDER BY random() LIMIT 1;";
  }
//  std::cout << requete << std::endl;
  
  Playlist laPlaylist(1, nom, std::chrono::minutes(duree));
//  std::cout << "playlist initialisé" << std::endl;

  laPlaylist.DoIt(requete);
//  std::cout << "DoIt effectuer" << std::endl;
  
  laPlaylist.writeM3U();
//  std::cout << "m3u créer" << std::endl;

  return 0;
}
