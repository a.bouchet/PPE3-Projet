#include "subgenre.h"
#include <iostream>

Subgenre::Subgenre():
  Subgenre(0, "")
{}

Subgenre::Subgenre(unsigned int _id, std::string _sub_type):
  id(_id),
  sub_type(_sub_type)
{}

Subgenre::~Subgenre() {}

unsigned int Subgenre::getId()
{
  return id;
}

std::string Subgenre::getSub_type()
{
  return sub_type;
}

void Subgenre::setSub_type(std::string _sub_type)
{
  sub_type = _sub_type;
}
